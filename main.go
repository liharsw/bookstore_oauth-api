package main

import (
	"gitlab.com/liharsw/bookstore_oauth-api/app"
)

func main() {
	app.StartApplication()
}
